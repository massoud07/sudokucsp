Sudoku backtrack with AC3 and Forward check algorithm.
The algorithm currently only works with easy level puzzles. 
A brutal level puzzle is also included under program, however it is commented out. 
If you wish to try the brutal level puzzle, comment the current code for the puzzle and uncomment the commented puzzle.