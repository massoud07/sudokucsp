﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SudokuCSP
{
	public class Board
	{
		public List<int[]> Rows { get; set; }
		public Dictionary<Tuple<int, int>, int> Prefills { get; }
		public Board()
		{
            Rows = new List<int[]>();

            InitBoard();		
		}

		public Board(Dictionary<Tuple<int,int>, int> prefills)
		{
            Rows = new List<int[]>();
            Prefills = prefills;

            InitBoard();
            AddPrefillToBoard();
        }

		public void PrintBoard() 
		{
			string output = "";

			for (int i = 0; i < Rows.Count (); i++) 
			{
				if (i == 3 || i == 6)
					output = output + "\n";
				
				output = output + string.Format ("{0} {1} {2}  {3} {4} {5}  {6} {7} {8}\n",
										        Rows [i][0], Rows [i][1], Rows [i][2], 
										        Rows [i][3], Rows [i][4], Rows [i][5], 
										        Rows [i][6], Rows [i][7], Rows [i][8]);
			}

            Console.CursorSize = 20;
            Console.WriteLine(output);
			return;
		}

		private void InitBoard()
		{
            for (int i = 0; i < 9; i++) 
			{
				Rows.Add (new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 });
			}

			return;
		}

		private void AddPrefillToBoard()
		{
            foreach (Tuple<int,int> key in Prefills.Keys)
            {
                Rows[key.Item1][key.Item2] = Prefills[key];
            }

			return;
		}
	}
}

