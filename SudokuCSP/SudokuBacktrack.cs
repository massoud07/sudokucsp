﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuCSP
{
    class SudokuSolver
    {
        private Board board;
        private int rowCount, colCount, rowStart, colStart;
        private Tuple<Tuple<int, int>, List<int>> variable;
        private List<Tuple<Tuple<int, int>, List<int>>> variableDomains;
        private Dictionary<Tuple<int, int>, int> assignedVariables { get; }
        private Random random;

        public SudokuSolver()
        {
            board = new Board();
            variable = new Tuple<Tuple<int, int>, List<int>>(new Tuple<int, int>(0, 0), new List<int>());
            variableDomains = new List<Tuple<Tuple<int, int>, List<int>>>();
            random = new Random(1);

            return;
        }

        public SudokuSolver(Board board)
        {
            variable = new Tuple<Tuple<int, int>, List<int>>(new Tuple<int, int>(0, 0), new List<int>());
            variableDomains = new List<Tuple<Tuple<int, int>, List<int>>>();
            assignedVariables = new Dictionary<Tuple<int, int>, int>();
            random = new Random(1);
            this.board = board;

            Init();

            return;
        }


        public bool SudokuAC3_FC()
        {
            Tuple<Tuple<int, int>, List<int>> Dx;

            if (assignedVariables.Count == 81)
            {
                foreach(Tuple<int,int> x in assignedVariables.Keys)
                {
                    board.Rows[x.Item1][x.Item2] = assignedVariables[x];
                }

                return true;
            }

            AC3();

            if (CheckEmptyDomains())
                return false;

			//Dx = variableDomains.First();
			Dx = variableDomains[MinimumRemainValuesIndex()];

            foreach(int x in Dx.Item2.ToList())
            {
                if (!assignedVariables.ContainsKey(Dx.Item1))
                {
                    assignedVariables.Add(Dx.Item1, x);
                    variableDomains.Remove(Dx);
                }

                ForwardCheck();

                if(!CheckEmptyDomains())
                {
                    bool result = SudokuAC3_FC();
                    if (result)
                        return result;

                    assignedVariables.Remove(Dx.Item1);
                    variableDomains.Add(Dx);
                }
            }

            return false;
        }

        public bool CheckConsistency(Tuple<Tuple<int, int>, List<int>> variable)
        {
            int c = 0;
            List<int> Dx = variable.Item2;
            rowCount = board.Rows.Count();
            colCount = board.Rows[0].Count();

            foreach (int x in Dx)
            {
                this.variable = new Tuple<Tuple<int, int>, List<int>>(new Tuple<int,int>(variable.Item1.Item1, variable.Item1.Item2), variable.Item2);

                if (CheckRowsAndColumns() && CheckSquare())
                    c++;
            }
            
            return (c > 0);
        }

		private int MinimumRemainValuesIndex()
		{
			int index = 0;

			for (int i = 0; i < variableDomains.Count; i++)
			{
				if (variableDomains[i].Item2.Count <= variableDomains[index].Item2.Count)
					index = i;
			}

			return index;
		}

        private bool CheckEmptyDomains()
        {
            bool empty = false;

            foreach (Tuple<Tuple<int, int>, List<int>> Dx in variableDomains)
            {
                if (Dx.Item2.Count == 0)
                    empty = true;
            }

            return empty;
        }

        private void AC3()
        {
            var Q = new List<Tuple<Tuple<int, int>, List<int>>>();
            Q = variableDomains.ToList(); 

            while (Q.Count > 0)
            {
                Tuple<Tuple<int, int>, List<int>> Dx = Q.First();

                List<Tuple<Tuple<int, int>, List <int>>> Y = GetRelatedVariables(Dx);

                Q.Remove(Dx);

                foreach(Tuple<Tuple<int, int>, List<int>> Dy in Y)
                {
                    if (Dx != Dy)
                    {
                        if (Revise(Dx, Dy))
                        {
                            if (Dy.Item2.Count == 0)
                                return;
                            else
                                Q.Add(Dy);
                        }
                    }
                }

            }
        }

        private List<Tuple<Tuple<int, int>, List<int>>> GetRelatedVariables(Tuple<Tuple<int, int>, List<int>> Dx)
        {
            List<Tuple<Tuple<int, int>, List<int>>> Y = new List<Tuple<Tuple<int, int>, List<int>>>();

            this.variable = Dx;
            SetSquareRowAndColStart();

            foreach (Tuple<Tuple<int, int>, List<int>> Dy in variableDomains)
            {
                if (Dy.Item1.Item1 == Dx.Item1.Item1 || Dy.Item1.Item2 == Dx.Item1.Item2)
                    Y.Add(Dy);
                else
                {
                    for (int i = rowStart; i < rowStart + 3; i++)
                    {
                        for (int j = colStart; j < colStart + 3; j++)
                        {
                            if (Dy.Item1.Item1 == i && Dy.Item1.Item2 == j)
                                Y.Add(Dy);
                        }
                    }
                }
            }

            return Y;
        }

        private void ForwardCheck()
        {
            rowCount = board.Rows.Count();
            colCount = board.Rows[0].Count();

            foreach (Tuple<Tuple<int, int>, List<int>> Dx in variableDomains)
            {
                this.variable = Dx;

                ForwardCheckRowAndColumn();
                ForwardCheckSquare();
            }

            return;
        }

        private void ForwardCheckRowAndColumn()
        {
            Tuple<int, int> varPos = variable.Item1;
            int value = board.Rows[varPos.Item1][varPos.Item2];

            for (var i = 0; i < colCount; i++)
            {
                var rowKey = new Tuple<int, int>(varPos.Item1, i);
                var colKey = new Tuple<int, int>(i, varPos.Item2);

                if (assignedVariables.ContainsKey(rowKey) && variable.Item2.Contains(assignedVariables[rowKey]))
                    variable.Item2.Remove(assignedVariables[rowKey]);

                if (assignedVariables.ContainsKey(colKey) && variable.Item2.Contains(assignedVariables[colKey]))
                    variable.Item2.Remove(assignedVariables[colKey]);
            }

            return;
        }

        private void ForwardCheckSquare()
        {
            Tuple<int, int> varPos = variable.Item1;
            int value = board.Rows[varPos.Item1][varPos.Item2];

            SetSquareRowAndColStart();

            for (int i = rowStart; i < rowStart + 3; i++)
            {
                for (int j = colStart; j < colStart + 3; j++)
                {
                    var key = new Tuple<int, int>(i, j);

                    if (assignedVariables.ContainsKey(key) && variable.Item2.Contains(assignedVariables[key]))
                        variable.Item2.Remove(assignedVariables[key]);
                }
            }

            return;
        }

        private bool Revise(Tuple<Tuple<int, int>, List<int>> Dx, Tuple<Tuple<int, int>, List<int>> Dy)
        {
            bool delete = false;
            bool found = false;

            foreach(int y in Dy.Item2.ToList())
            {
                foreach(int x in Dx.Item2)
                {
                    if(y != x)
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    Dy.Item2.Remove(y);

                    delete = true;
                } 
            }

            return delete;
        }

        private void Init()
        {
            for(int i = 0; i < board.Rows.Count; i++)
            {
                for (int j = 0; j < board.Rows.Count; j++)
                {
                    var Dx = new Tuple<Tuple<int, int>, List<int>>(new Tuple<int,int>(i,j), new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }));

                    if (!board.Prefills.ContainsKey(Dx.Item1))
                    {
                        variableDomains.Add(Dx);
                    }
                    else
                    {
                        assignedVariables.Add(Dx.Item1, board.Prefills[Dx.Item1]);
                    }
                }
            }

            ForwardCheck();

            return;
        }

        private bool CheckRowsAndColumns()
        {
            Tuple<int, int> varPos = variable.Item1;

            if (assignedVariables.ContainsKey(varPos))
            {
                int value = assignedVariables[varPos];

                for (var i = 0; i < colCount; i++)
                {
                    var rowKey = new Tuple<int, int>(varPos.Item1, i);
                    var colKey = new Tuple<int, int>(i, varPos.Item2);

                    if ((rowKey != varPos && assignedVariables.ContainsKey(rowKey) && assignedVariables[rowKey] == value) ||
                        (colKey != varPos && assignedVariables.ContainsKey(colKey) && assignedVariables[colKey] == value))
                        return false;
                }
            }

            return true;
        }

        private bool CheckSquare()
        {
            Tuple<int, int> varPos = variable.Item1;

            SetSquareRowAndColStart();

            if (assignedVariables.ContainsKey(varPos))
            {
                int value = assignedVariables[varPos];

                for (int i = rowStart; i < rowStart + 3; i++)
                {
                    for (int j = colStart; j < colStart + 3; j++)
                    {
                        var key = new Tuple<int, int>(i, j);

                        if (varPos != key)
                        {
                            if (assignedVariables.ContainsKey(key) && assignedVariables[key] == value)
                                return false;
                        }
                    }
                }
            }
			
            return true;
        }

		private void SetSquareRowAndColStart()
		{
            Tuple<int, int> varPos = variable.Item1;

            if (varPos.Item1 >= 0 && varPos.Item1 <= 2)
				rowStart = 0;
			else if (varPos.Item1 >= 3 && varPos.Item1 <= 5)
				rowStart = 3;
			else if (varPos.Item1 >= 6 && varPos.Item1 <= 8)
				rowStart = 6;

			if (varPos.Item2 >= 0 && varPos.Item2 <= 2)
				colStart = 0;
			else if (varPos.Item2 >= 3 && varPos.Item2 <= 5)
				colStart = 3;
			else if (varPos.Item2 >= 6 && varPos.Item2 <= 8)
				colStart = 6;
		}
    }
}
