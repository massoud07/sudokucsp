﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
//using Microsoft.SolverFoundation.Services;
//using Microsoft.SolverFoundation.Common;

namespace SudokuCSP
{
    class Program
    {
        static Dictionary<Tuple<int, int>, int> GetPrefills()
        {
            var prefills = new Dictionary<Tuple<int, int>, int>();
			int c = 0, r = 0;
			string[] digits;
            string path = Path.Combine(Environment.CurrentDirectory, @"puzzles\", "puzzle.txt");

			if (System.IO.File.Exists(path))
			{
				var puzzle = System.IO.File.ReadLines(path);

				foreach (string row in puzzle)
				{
					if (!string.IsNullOrEmpty(row))
					{
						digits = row.Split(' ');
						foreach (string digit in digits)
						{
							if (!string.IsNullOrEmpty(digit))
							{
								if (!digit.Equals("0"))
								{
									prefills.Add(new Tuple<int, int>(r, c), Convert.ToInt32(digit));
								}
								c++;
							}
						}

						r++;
						c = 0;
					}
				}
			}

            //prefills.Add(new Tuple<int, int>(0, 0), 4);
            //prefills.Add(new Tuple<int, int>(0, 2), 2);
            //prefills.Add(new Tuple<int, int>(0, 4), 8);
            //prefills.Add(new Tuple<int, int>(0, 5), 7);
            //prefills.Add(new Tuple<int, int>(0, 8), 3);

            //prefills.Add(new Tuple<int, int>(1, 1), 6);
            //prefills.Add(new Tuple<int, int>(1, 4), 1);
            //prefills.Add(new Tuple<int, int>(1, 5), 2);
            //prefills.Add(new Tuple<int, int>(1, 7), 8);

            //prefills.Add(new Tuple<int, int>(2, 3), 3);

            //prefills.Add(new Tuple<int, int>(3, 0), 3);
            //prefills.Add(new Tuple<int, int>(3, 2), 1);
            //prefills.Add(new Tuple<int, int>(3, 5), 5);
            //prefills.Add(new Tuple<int, int>(3, 6), 2);
            //prefills.Add(new Tuple<int, int>(3, 7), 9);

            //prefills.Add(new Tuple<int, int>(4, 0), 5);
            //prefills.Add(new Tuple<int, int>(4, 3), 2);
            //prefills.Add(new Tuple<int, int>(4, 4), 6);
            //prefills.Add(new Tuple<int, int>(4, 5), 3);
            //prefills.Add(new Tuple<int, int>(4, 8), 7);

            //prefills.Add(new Tuple<int, int>(5, 1), 2);
            //prefills.Add(new Tuple<int, int>(5, 2), 8);
            //prefills.Add(new Tuple<int, int>(5, 3), 9);
            //prefills.Add(new Tuple<int, int>(5, 6), 5);
            //prefills.Add(new Tuple<int, int>(5, 8), 4);

            //prefills.Add(new Tuple<int, int>(6, 5), 9);

            //prefills.Add(new Tuple<int, int>(7, 1), 9);
            //prefills.Add(new Tuple<int, int>(7, 3), 6);
            //prefills.Add(new Tuple<int, int>(7, 4), 2);
            //prefills.Add(new Tuple<int, int>(7, 7), 5);


            //prefills.Add(new Tuple<int, int>(8, 0), 1);
            //prefills.Add(new Tuple<int, int>(8, 3), 7);
            //prefills.Add(new Tuple<int, int>(8, 4), 5);
            //prefills.Add(new Tuple<int, int>(8, 6), 6);
            //prefills.Add(new Tuple<int, int>(8, 8), 9);

            ///////////////////////////////////////////


            //prefills.Add(new Tuple<int, int>(0, 2), 2);
            //prefills.Add(new Tuple<int, int>(0, 3), 7);
            //prefills.Add(new Tuple<int, int>(0, 8), 1);

            //prefills.Add(new Tuple<int, int>(1, 2), 9);
            //prefills.Add(new Tuple<int, int>(1, 3), 4);
            //prefills.Add(new Tuple<int, int>(1, 4), 5);

            //prefills.Add(new Tuple<int, int>(2, 5), 8);
            //prefills.Add(new Tuple<int, int>(2, 7), 3);
            //prefills.Add(new Tuple<int, int>(2, 8), 7);

            //prefills.Add(new Tuple<int, int>(3, 3), 5);
            //prefills.Add(new Tuple<int, int>(3, 6), 1);
            //prefills.Add(new Tuple<int, int>(3, 7), 9);

            //prefills.Add(new Tuple<int, int>(4, 1), 6);
            //prefills.Add(new Tuple<int, int>(4, 7), 2);

            //prefills.Add(new Tuple<int, int>(5, 1), 9);
            //prefills.Add(new Tuple<int, int>(5, 2), 3);
            //prefills.Add(new Tuple<int, int>(5, 5), 4);


            //prefills.Add(new Tuple<int, int>(6, 0), 8);
            //prefills.Add(new Tuple<int, int>(6, 1), 7);
            //prefills.Add(new Tuple<int, int>(6, 3), 6);


            //prefills.Add(new Tuple<int, int>(7, 4), 1);
            //prefills.Add(new Tuple<int, int>(7, 5), 5);
            //prefills.Add(new Tuple<int, int>(7, 6), 4);

            //prefills.Add(new Tuple<int, int>(8, 0), 9);
            //prefills.Add(new Tuple<int, int>(8, 5), 7);
            //prefills.Add(new Tuple<int, int>(8, 6), 8);



			//***************************************//

			//prefills.Add(new Tuple<int, int>(0, 2), 6);
			//prefills.Add(new Tuple<int, int>(0, 3), 3);
			//prefills.Add(new Tuple<int, int>(0, 4), 8);
			//prefills.Add(new Tuple<int, int>(0, 6), 4);
			//prefills.Add(new Tuple<int, int>(0, 8), 1);

			//prefills.Add(new Tuple<int, int>(1, 0), 8);
			//prefills.Add(new Tuple<int, int>(1, 2), 4);
			//prefills.Add(new Tuple<int, int>(1, 5), 9);
			//prefills.Add(new Tuple<int, int>(1, 6), 2);
			//prefills.Add(new Tuple<int, int>(1, 7), 5);

			//prefills.Add(new Tuple<int, int>(2, 2), 2);

			//prefills.Add(new Tuple<int, int>(3, 0), 6);
			//prefills.Add(new Tuple<int, int>(3, 4), 3);
			//prefills.Add(new Tuple<int, int>(3, 5), 4);

			//prefills.Add(new Tuple<int, int>(4, 3), 8);
			//prefills.Add(new Tuple<int, int>(4, 5), 6);

			//prefills.Add(new Tuple<int, int>(5, 3), 9);
			//prefills.Add(new Tuple<int, int>(5, 4), 7);
			//prefills.Add(new Tuple<int, int>(5, 8), 2);

			//prefills.Add(new Tuple<int, int>(6, 6), 8);

			//prefills.Add(new Tuple<int, int>(7, 1), 2);
			//prefills.Add(new Tuple<int, int>(7, 2), 3);
			//prefills.Add(new Tuple<int, int>(7, 3), 1);
			//prefills.Add(new Tuple<int, int>(7, 6), 5);
			//prefills.Add(new Tuple<int, int>(7, 8), 6);

			//prefills.Add(new Tuple<int, int>(8, 0), 5);
			//prefills.Add(new Tuple<int, int>(8, 2), 7);
			//prefills.Add(new Tuple<int, int>(8, 4), 9);
			//prefills.Add(new Tuple<int, int>(8, 5), 3);
			//prefills.Add(new Tuple<int, int>(8, 6), 1);


            return prefills;
        }
        static void Main(string[] args)
        {
            var board = new Board(GetPrefills());
            var solver = new SudokuSolver(board);
            var variable = new Tuple<int, int>(7, 5);

            board.PrintBoard();

            //Console.WriteLine(solver.CheckConsistency(variable));

            Console.WriteLine(solver.SudokuAC3_FC());

            board.PrintBoard();

            string input = Console.ReadLine();
        }
    }
}
